import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { categoriesArr } from "../../assets/data";
import { levelsArr } from "../../assets/data";
import axios from "axios";

const initialState = {
    categoriesData: categoriesArr,
    levelsData: levelsArr,
    currentCategory: '',
    currentLevel: '',
    questionsData: [],
    fetchingId: '',
    currentQuestion: 0,
    answersData: [],
    resultsData: [],
    isLoading: false
}

export const getQuestions = createAsyncThunk('quiz/getQuestions',
    async (fetchDetails, asyncThunk) => {
        try {
            const response = await axios(`https://opentdb.com/api.php?amount=10&category=${fetchDetails.category}&difficulty=${fetchDetails.level.toLowerCase()}&type=multiple`)
            console.log(response)
            return response.data
        } catch (error) {
            return asyncThunk.rejectWithValue('Something wrong on fetching Data')
        }
    }
)

export const quizSlice = createSlice({
    name: 'quiz',
    initialState,
    reducers: {
        onChangeCategory: (state, { payload }) => {
            state.currentCategory = payload
        },
        onChangeLevel: (state, { payload }) => {
            state.currentLevel = payload
        },
        onChangeFetchId: (state, { payload }) => {
            state.fetchingId = payload
        },
        onChangeQuestionNext: (state) => {
            if (state.currentQuestion !== 9) {
                state.currentQuestion = state.currentQuestion + 1
            } else {
                state.currentQuestion = 9
            }
        },
        onStoreAnswer: (state, { payload }) => {
            state.answersData[state.currentQuestion].userAnswer = payload.clickedAnswer
        },
        calculatingResults: (state) => {
            let countCorrect = 0
            let answersObject = {}
            let itemId = 1
            state.answersData.map(eachAnswer => {
                if (eachAnswer.correct_answer === eachAnswer.userAnswer) {
                    countCorrect++
                    answersObject[eachAnswer.question] = {
                        id: itemId,
                        correctAnswer: eachAnswer.correct_answer,
                        userSelectedAnswer: eachAnswer.userAnswer,
                        status: true
                    }
                } else {
                    answersObject[eachAnswer.question] = {
                        id: itemId,
                        correctAnswer: eachAnswer.correct_answer,
                        userSelectedAnswer: eachAnswer.userAnswer,
                        status: false
                    }
                }
                itemId++
            })
            state.resultsData.push({ score: countCorrect, result: answersObject })
        },
        resetGame: (state) => {
            state.currentQuestion = 0
            state.questionsData = []
            state.answersData = []
        }
    },
    extraReducers: {
        [getQuestions.pending]: (state, { payload }) => {
            state.questionsData = []
            state.isLoading = true
        },
        [getQuestions.fulfilled]: (state, { payload }) => {
            state.questionsData = payload.results
            state.answersData = payload.results
            state.isLoading = false
        },
        [getQuestions.rejected]: (state, { payload }) => {
            state.questionsData = []
            state.isLoading = false
        }
    }
})

export const { onChangeCategory, onChangeLevel, onChangeFetchId,
    onChangeQuestionNext, onStoreAnswer, calculatingResults, resetGame } = quizSlice.actions

export default quizSlice.reducer