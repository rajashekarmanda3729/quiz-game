import React from 'react'
import Answer from './Answer'
import { useDispatch, useSelector } from 'react-redux'
import { calculatingResults, onChangeQuestionNext, onStoreAnswer } from '../redux/quiz/quizSlice'
import { Link } from 'react-router-dom'

const Question = ({ questionDetails }) => {

    const dispatch = useDispatch()
    const { currentQuestion, answersData } = useSelector(store => store.quiz)
    const { question, correct_answer, incorrect_answers } = questionDetails
    let answers = incorrect_answers

    let clickedAnswerWord = answersData[currentQuestion].userAnswer
    let selectedAnswer = answersData[currentQuestion].hasOwnProperty('userAnswer')

    const onChangeAnswer = (event) => {
        const clickedAnswer = event.target.value
        dispatch(onStoreAnswer({ question: question, clickedAnswer: clickedAnswer }))
    }

    return (
        <section className='d-flex flex-column shadow-lg rounded-4 p-3 h-75 w-75 position-relative'>
            <h3>{`${currentQuestion + 1}).`}{question}?</h3>
            <section className='d-flex flex-column h-50 mt-3'>
                <button className={`${clickedAnswerWord == correct_answer ? 'btn-dark' : ''} answer btn btn-warning w-50 m-2 text-start`}
                    value={correct_answer} onClick={onChangeAnswer}>
                    {`1). `} {correct_answer}
                </button>
                <div className="d-flex flex-column h-100">
                    {
                        answers && answers.map(eachAnswer => {
                            return <Answer key={eachAnswer} answer={eachAnswer} question={question} number={answers.indexOf(eachAnswer)} />
                        })
                    }
                </div>
            </section>
            <div className="d-flex justify-content-end align-items-end mt-5 p-5 h-25 position-absolute end-0 bottom-0">
                <Link to={`${currentQuestion == 9 ? '/category/level/resultspage' : '/category/level'}`}>
                    <button className={`btn btn-${currentQuestion != 9 ? 'dark' : 'success'}`}
                        onClick={() => {
                            selectedAnswer && dispatch(onChangeQuestionNext())
                            !selectedAnswer && alert('Please choose option')
                            currentQuestion == 9 && dispatch(calculatingResults())
                        }}>
                        {currentQuestion != 9 ? 'Next One' : 'Submit'}</button>
                </Link>
            </div>
        </section>
    )
}

export default Question