import { useDispatch, useSelector } from 'react-redux'
import Level from './Level'

const GameLevels = () => {

    const { levelsData, currentCategory } = useSelector(store => store.quiz)

    return (
        <main className='d-flex flex-column justify-content-start mt-5 align-items-center h-100'>
            <h2 className='text-danger'>Welcome to {currentCategory} Quiz</h2>
            <h3 className='text-dark'>Here we have three difficulty levels,Go with each one and test your knowledge</h3>
            <h3 className="text-success mb-5">Select the level of Quiz</h3>
            <section className='d-flex flex-row justify-content-center align-items-center'>
                {
                    levelsData && levelsData.map(eachLevel => {
                        return <Level key={eachLevel.id} levelDetails={eachLevel} />
                    })
                }
            </section>
        </main>
    )
}

export default GameLevels