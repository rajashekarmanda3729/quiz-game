import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
    return (
        <div className='d-flex flex-column w-100 h-100 justify-content-center align-items-center'>
            <img
                src="https://assets.ccbp.in/frontend/react-js/not-found-blog-img.png"
                alt="not found"
                className="w-25 h-50"
            />
            <h1>No Data Found</h1>
            <Link to='/'>
                <button className="btn btn-danger">Go to home</button>
            </Link>
        </div>
    )
}

export default NotFound