import React from 'react'
import {AiFillCheckCircle} from 'react-icons/ai'
import {AiFillCloseCircle} from 'react-icons/ai'

const TableRow = ({question,details}) => {
    const {id,correctAnswer,userSelectedAnswer,status} = details
    return (
        <tr className='text-center'>
            <td scope="row">{question}</td>
            <td>{correctAnswer}</td>
            <td>{userSelectedAnswer}</td>
            <td>{status ? <AiFillCheckCircle size={26} color='green'/> : <AiFillCloseCircle size={26} color='red'/>}</td>
        </tr>
    )
}

export default TableRow