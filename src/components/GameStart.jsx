import React from 'react'
import Category from './Category'
import { useSelector } from 'react-redux'

const GameStart = () => {

  const { categoriesData } = useSelector(store => store.quiz)

  return (
    <main className='d-flex flex-column h-75 justify-content-center align-items-center'>
      <h2 className='text-center text-success mb-0'>Welcome to Quiz Application</h2>
      <section className='w-100 flex-wrap  d-flex justify-content-center align-items-center'>
        {
          categoriesData && categoriesData.map(eachCategory => {
            return <Category key={eachCategory.id} categoryDetails={eachCategory} />
          })
        }
      </section>
    </main>
  )
}

export default GameStart