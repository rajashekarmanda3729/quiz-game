import React, { Suspense, useEffect } from 'react'
import Question from './Question'
import { useDispatch, useSelector } from 'react-redux'
import { getQuestions } from '../redux/quiz/quizSlice'
import { Oval } from 'react-loader-spinner'
import NotFound from './NotFound'

const QuestionsPage = () => {

    const dispatch = useDispatch()
    const { isLoading, currentLevel, fetchingId, currentCategory,
        questionsData, currentQuestion } = useSelector(store => store.quiz)

    useEffect(() => {
        dispatch(getQuestions({ category: fetchingId, level: currentLevel }))
    }, [])

    return (
        <main className='d-flex h-100 flex-column'>
            {
                !isLoading ?
                    (
                        questionsData.length != 0 ?
                            <>
                                <section className="d-flex justify-content-evenly align-items-center mt-5">
                                    <button className="btn btn-warning w-25 fs-5">Quiz Category : <span className='text-success fs-5'>{currentCategory}</span></button>
                                    <button className="btn btn-warning w-25 fs-5">Quiz Level : <span className='text-success fs-5'>{currentLevel}</span></button>
                                </section>
                                <section className='d-flex flex-column mt-5 justify-content-center align-items-center'>
                                    <div className="d-flex w-75">
                                        <p>Question {currentQuestion + 1}/10</p>
                                    </div>
                                    <input type="range" min={0} value={currentQuestion + 1} max={10} className='w-75 range' />
                                </section>
                                <section className='d-flex justify-content-center align-items-center h-75'>
                                    {questionsData.length > 0 && <Question questionDetails={questionsData[currentQuestion]} />}
                                </section>
                            </> :
                            <NotFound />
                    ) :
                    <section className='d-flex justify-content-center align-items-center h-50'>
                        <Oval height={80} width={80} color="#4fa94d" wrapperStyle={{}} wrapperClass="" visible={true}
                            ariaLabel='oval-loading'
                            secondaryColor="#4fa94d"
                            strokeWidth={4}
                            strokeWidthSecondary={4}
                        />
                    </section>
            }
        </main>
    )
}

export default QuestionsPage