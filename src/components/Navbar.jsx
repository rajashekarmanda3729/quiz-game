import React from 'react'
import { BiBrain } from 'react-icons/bi'
import { AiFillHome } from 'react-icons/ai'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { resetGame } from '../redux/quiz/quizSlice'

const Navbar = () => {

    const dispatch = useDispatch()

    return (
        <nav className="navbar bg-body-tertiary bg-warning bg-opacity-75 p-2 w-100">
            <div className="d-flex flex-row justify-content-center align-items-center ms-2">
                <Link to='/' className='text-decoration-none' onClick={() => dispatch(resetGame())}>
                    <h3 className='text-success'><BiBrain color='green' size={38} />Quiz Game</h3>
                </Link>
            </div>
            <Link to='/' onClick={() => dispatch(resetGame())}>
                <AiFillHome color='green' size={30} className='me-2' />
            </Link>
        </nav>
    )
}

export default Navbar