import React from 'react'
import '../style/style.css'
import { useDispatch, useSelector } from 'react-redux'
import { onStoreAnswer } from '../redux/quiz/quizSlice'

const Answer = ({ answer, question, number }) => {

    const dispatch = useDispatch()
    const { answersData, currentQuestion } = useSelector(store => store.quiz)
    let clickedAnswerWord = answersData[currentQuestion].userAnswer

    const onChangeAnswer = (event) => {
        const clickedAnswer = event.target.value
        dispatch(onStoreAnswer({ question: question, clickedAnswer: clickedAnswer }))
    }

    return (
        <button value={answer} onClick={onChangeAnswer}
            className={`${clickedAnswerWord == answer ? 'btn-dark' : ''} answer btn btn-warning h-25 w-50 m-2 text-start`}>
            {`${number + 2}). `}{answer}
        </button>
    )
}

export default Answer