import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { onChangeLevel } from '../redux/quiz/quizSlice'
import { Link, useParams } from 'react-router-dom'
import '../style/style.css'

const Level = ({ levelDetails }) => {

    const { category } = useParams()
    const { currentCategory } = useSelector(store => store.quiz)
    const dispatch = useDispatch()
    const { name,bgColor } = levelDetails

    return (
        <div className={`level h-100 w-50 m-3 p-3 d-flex flex-column ${bgColor} rounded-4 justify-content-center align-items-center`}>
            <h2 className='text-dark'><span className='text-light'> {name}</span> Level </h2>
            <h5 className='text-dark text-center'>This quiz will test you the knowledge of {currentCategory}</h5>
            <Link to={`/${category}/${name}`}>
                <button className="level-button btn btn-dark"
                    onClick={() => dispatch(onChangeLevel(name))}>Try With This</button>
            </Link>
        </div>
    )
}

export default Level