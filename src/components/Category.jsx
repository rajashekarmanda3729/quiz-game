import React from 'react'
import { FaRunning } from 'react-icons/fa'
import { GrPersonalComputer } from 'react-icons/gr'
import { SiStartrek } from 'react-icons/si'
import { ImHourGlass } from 'react-icons/im'
import '../style/style.css'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { getQuestions, onChangeCategory, onChangeFetchId } from '../redux/quiz/quizSlice'

const Category = ({ categoryDetails }) => {

    const { currentLevel } = useSelector(store => store.quiz)
    const { color, name, fetchId } = categoryDetails
    const dispatch = useDispatch()

    return (
        <div className="d-flex flex-row rounded-4 shadow-lg quiz-card">
            <div className={`col-4 bg-${color} text-center rounded-start d-flex justify-content-center align-items-center`}>
                {name == 'Computers' && <GrPersonalComputer color='white' size={160} className='' />}
                {name == 'Sports' && <FaRunning color='white' size={'12rem'} className='' />}
                {name == 'Arts' && <SiStartrek color='white' size={160} className='' />}
                {name == 'Politics' && <ImHourGlass color='white' size={160} className='' />}
            </div>
            <div className="col-8 ps-5 d-flex justify-content-center align-items-center">
                <div className="card-body">
                    <h2 className="card-title">Quiz on {name}</h2>
                    <p className="card-text fs-5">This quiz will test you the knowledge of <span className='text-danger'>{name}</span></p>
                    <Link to={`/${name}`}>
                        <button className="category-button btn btn-dark"
                            onClick={() => {
                                dispatch(onChangeCategory(name))
                                dispatch(onChangeFetchId(fetchId))
                            }}>Take This Quiz</button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Category