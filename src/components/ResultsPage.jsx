import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { calculatingResults } from '../redux/quiz/quizSlice'
import TableRow from './TableRow'
import { GiTrophyCup } from 'react-icons/gi'
import NotFound from './NotFound'

const ResultsPage = () => {

    const { currentCategory, currentLevel, resultsData } = useSelector(store => store.quiz)
    const resultsObjects = resultsData[0].result
    const totalScore = resultsData[0].score

    return (
        <main className='d-flex flex-column border h-100'>
            <section className='d-flex flex-column justify-content-center align-items-center mt-4'>
                <div className='win-box bg-success p-3 rounded-4 justify-content-center align-items-center'>
                    <GiTrophyCup className='w-100 h-100' color='white' />
                </div>
                <h1 className='text-secondary'>Quiz Result</h1>
                <h3 className='text-success'>Quiz Category: <span className='text-danger'>{currentCategory}</span></h3>
                <h3 className='text-success'>Quiz Difficulty Level: <span className='text-danger'>{currentLevel}</span></h3>
                <h3 className='text-secondary'>Final Score: <span className='text-warning'>{totalScore}</span></h3>
            </section>
            <section className='d-flex p-5 justify-content-center align-items-center'>
                <table className="table table-bordered border-primary border-2 w-75">
                    <thead>
                        <tr className='border border-danger text-center'>
                            <th scope="col">Question</th>
                            <th scope="col">Correct Answer</th>
                            <th scope="col">You Selected</th>
                            <th scope="col">Right or Wrong</th>
                        </tr>
                    </thead>
                    <tbody className=''>
                        {
                            resultsObjects ?
                                 Object.entries(resultsObjects).map(([question, details]) => {
                                    return <TableRow key={details.id} question={question} details={details} />
                                }) :
                                <NotFound />
                        }
                    </tbody>
                </table>

            </section>
        </main>
    )
}

export default ResultsPage

