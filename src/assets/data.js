import { FaRunning } from 'react-icons/fa'
import { GrPersonalComputer } from 'react-icons/gr'
import { SiStartrek } from 'react-icons/si'
import { ImHourGlass } from 'react-icons/im'

export const categoriesArr = [
    {
        id: 1,
        name: 'Computers',
        color: 'success',
        fetchId: 18
    },
    {
        id: 2,
        name: 'Sports',
        color: 'warning',
        fetchId: 21
    },
    {
        id: 3,
        name: 'Arts',
        color: 'secondary',
        fetchId: 25
    },
    {
        id: 4,
        name: 'Politics',
        color: 'primary',
        fetchId: 24
    },
]

export const levelsArr = [
    {
        id: 1,
        name: 'Easy',
        bgColor:'bg-success'
    },
    {
        id: 2,
        name: 'Medium',
        bgColor:'bg-warning'
    },
    {
        id: 3,
        name: 'Hard',
        bgColor:'bg-danger'
    }
]