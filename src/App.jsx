import Navbar from './components/Navbar'
import GameStart from './components/GameStart'
import GameLevels from './components/GameLevels'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import QuestionsPage from './components/QuestionsPage'
import ResultsPage from './components/ResultsPage'
import { Suspense } from 'react'
import NotFound from './components/NotFound'

function App() {

  return (
    <main className='vh-100 vw-100 d-flex flex-column'>
      <Suspense fallback='Loading...'>
        <BrowserRouter>
          <Navbar />
          <Routes>
            <Route path='/' element={<GameStart />} />
            <Route path='/:category' element={<GameLevels />} />
            <Route path='/:category/:level' element={<QuestionsPage />} />
            <Route path='/category/level/resultspage' element={<ResultsPage />} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </Suspense>
    </main>
  )
}

export default App
