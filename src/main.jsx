import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css'
import { store } from './redux/store/store.js'
import { Provider } from 'react-redux'
import ErrorBoundary from './components/ErrorBoundary.jsx'
import NotFound from './components/NotFound.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <ErrorBoundary fallback='Page Not Found, Error'>
    <Provider store={store}>
      <React.StrictMode>
        <App />
      </React.StrictMode>,
    </Provider>
  </ErrorBoundary>
)
